use async_broadcast::{Sender,Receiver};
use crate::information::{Data,Frame};

/*
Leer settings desde calibracion
Es una matriz calibrada a la posicion actual.

*/


pub async fn read_video(
	_gui_tx:Sender<Data>, 
	_gui_rx:Receiver<Data>, 
	_process_tx:Sender<Frame>, 
	_process_rx:Receiver<Frame>) {
	println!("Reading video")
}
