use async_broadcast::{Sender,Receiver};
use crate::information::{Action, OutputChannel};
use tokio::time::{sleep_until, Instant};
use std::time::Duration;
use std::clone::Clone;

const TIME_SLEEP: u8 = 1;

pub async fn control_lights<T: OutputChannel + Clone + std::fmt::Display + std::fmt::Debug>(
	mut control_rx:Receiver<Action<T>>) {
	/*
	read action from channel
	activate specific action based on Action instance
	 */
	println!("Control lights");
	let time_sleep = TIME_SLEEP;

	loop {
		let until = Instant::now() + Duration::from_secs(time_sleep.into());
		println!("Control IN LOOP cap recv {:}, msgs: {:?}",
				 control_rx.capacity(), control_rx.len());
		// mientras no este vacio tomar los datos
		while !control_rx.is_empty() {
			match control_rx.recv().await {
				Ok(value) => {
					println!("Recibido en control: {:?}",value)},
				Err(err) => println!("Error {:}",err)				
			};
		};

		sleep_until(until).await;		
	}
}
