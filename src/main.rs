use shakespierre::calibration::gui;
use shakespierre::readvideo::read::read_video;
use shakespierre::processimage::process::{process_image, SemaphorColor};
use shakespierre::controllights::control::control_lights;
use shakespierre::information::{Data,Frame,Action};

use futures::future::{self};
use async_broadcast::{broadcast};//, TryRecvError, TrySendError};


#[tokio::main]
async fn main() {
	/*
	Esta estructura permitira ejecutar una interfaz de control en una
	cpu y el procesamiento de imagenes en otra.
	 */
	let (tx_gui2read, rx_gui2read) =  broadcast::<Data>(16);
	let (tx_read2process, rx_read2process) =  broadcast::<Frame>(256);
	let (mut tx_process2control, mut rx_process2control) = broadcast::<Action<SemaphorColor>>(256);
	
	let rx_gui2read_1 = rx_gui2read.clone();
	let tx_gui2read_1 = tx_gui2read.clone();

	let run_gui = tokio::spawn(async move {
		// insert sender broadcast 
		// insert receiver broadcast
			gui::run(tx_gui2read_1, rx_gui2read_1).await;
		});

	// connect gui with video reader
	let rx_gui2read_2 = rx_gui2read.clone();
	let tx_gui2read_2 = tx_gui2read.clone();

	// connect gui with image process
	let tx_read2process_1 = tx_read2process.clone();
	let rx_read2process_1 = rx_read2process.clone();

	// read to process
	let tx_read2process_2 = tx_read2process.clone();
	let rx_read2process_2 = rx_read2process.clone();

	// process to control

	
	let run_video = tokio::spawn(async move {
			read_video(
				tx_gui2read_2, 
				rx_gui2read_2,
				tx_read2process_1, 
				rx_read2process_1).await; 
	});

	let run_process_image = tokio::spawn(async move {
			process_image(
				tx_read2process_2,
				rx_read2process_2,
				tx_process2control,
			).await;
	});

	let run_control = tokio::spawn(async move {
			control_lights(
				rx_process2control).await;
		});

	let _outputs = future::try_join_all(
		vec![run_gui, run_video, run_process_image, run_control]).await;

}
