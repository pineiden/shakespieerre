use async_broadcast::{Sender,Receiver};
use crate::information::{Frame,Action, OutputChannel};
use tokio::time::{sleep_until, Instant};
use std::time::Duration;
use chrono::{Utc};
use chrono::Timelike;
use std::fmt;
use std::clone::Clone;

const TIME_SLEEP: u8 = 1;

#[derive(Debug, Clone)]
pub enum SemaphorColor {
	White,
	Green,
	Yellow,
	Red,
}

impl fmt::Display for SemaphorColor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
       match *self {
           SemaphorColor::White => write!(f, "Blanco"),
           SemaphorColor::Green => write!(f, "Verde"),
		   SemaphorColor::Yellow => write!(f, "Amarillo"),
		   SemaphorColor::Red => write!(f, "Rojo")
       }
    }
}

impl OutputChannel for SemaphorColor { 
    fn get_channel(&self) -> String {
       match *self {
           SemaphorColor::White => String::from("White"),
           SemaphorColor::Green => String::from("Green"),
		   SemaphorColor::Yellow => String::from("Yellow"),
		   SemaphorColor::Red => String::from("Red"),
       }
    }
}


async fn estimate_time() -> Action<SemaphorColor> {
	// obtener segundos desde now.
	let now = Utc::now();
	let seconds = now.second();
	println!("Est {:?}", seconds);
	let code = String::from("foco");
	match seconds {
		0..=30 => 	Action::new(&code,SemaphorColor::White),
		31..=40 =>  Action::new(&code,SemaphorColor::Green),
		41..=50 =>  Action::new(&code,SemaphorColor::Yellow),
		_ => Action::new(&code,SemaphorColor::Red)
	}
}

pub async fn process_image(
	_process_tx:Sender<Frame>, 
	_process_rx:Receiver<Frame>, 
	control_tx:Sender<Action<SemaphorColor>>,
 ) {
	println!("Process image");
	/*
		Get time
		compare seconds
		in condition send Action
	 */

	let time_sleep = TIME_SLEEP;

	loop {
		let mut until = Instant::now() + Duration::from_secs(time_sleep.into());
		let action: Action<SemaphorColor> = estimate_time().await;
		// estimate value
		println!("Process IN LOOP {:#?}", action);

		// si esta lleno esperar a que se vacie
		while control_tx.is_full() {
			sleep_until(until).await;
			until = Instant::now() + Duration::from_secs(time_sleep.into());
		};

		match control_tx.broadcast(action).await {
			Ok(value) => println!("Sended value OK {:?}", value),
			Err(err) => {println!("Error {:}", err)}
		};

		sleep_until(until).await;
	}
}
