use serde::{Deserialize, Serialize};
use std::clone::Clone;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Data {
	pub command: String
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Frame {
	pub origin: String

}


pub trait OutputChannel {
	fn get_channel(&self) -> String;
}  

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Action<T> where T: OutputChannel + Clone  {
	pub code: String,
	pub channel: T
}



impl<T> Action<T> where T: OutputChannel + Clone{
	pub fn new(code: &String, channel: T) -> Self {
		Action {code:code.to_string(), channel}
	}
}
