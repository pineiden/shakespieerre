# Proyecto Shakespierre: amar�s el teatro

Este proyecto consiste en un sistema que controle la iluminación de zonas y 
se activen una vez se cumpla alguna condición definida por calibración.

Debe consistir, en general, en las siguientes etapas:

- calibración
- leer video
- procesar imagen
- activar o no foco (id foco)

En el escenario, la persona podrá acceder a cierta zona predefinida e 
iluminada con IR y activar la luz con cierto enfoque.
